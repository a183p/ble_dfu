package com.example.user.bledfu;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import no.nordicsemi.android.dfu.DfuProgressListener;
import no.nordicsemi.android.dfu.DfuServiceController;
import no.nordicsemi.android.dfu.DfuServiceInitiator;
import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanCallback;
import no.nordicsemi.android.support.v18.scanner.ScanResult;

/**
 * Created by user on 2017/5/12.
 */

public class BleServices {
    private ScanCallback ble_scan_callback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            myble_callback.scaned_devices(result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
        }
    };
    private BluetoothGattCallback ble_gatt_callback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Toast.makeText(context, "connect_success", Toast.LENGTH_SHORT);
                Log.d("connect", "success");
            } else if (status == BluetoothGatt.GATT_FAILURE) {
                Toast.makeText(context, "connect_failure", Toast.LENGTH_SHORT);
                Log.d("connect", "failure");
            }
        }
    };
    private final DfuProgressListener mDfuProgressListener = new DfuProgressListener() {
        @Override
        public void onDeviceConnecting(String deviceAddress) {

        }

        @Override
        public void onDeviceConnected(String deviceAddress) {

        }

        @Override
        public void onDfuProcessStarting(String deviceAddress) {

        }

        @Override
        public void onDfuProcessStarted(String deviceAddress) {

        }

        @Override
        public void onEnablingDfuMode(String deviceAddress) {

        }

        @Override
        public void onProgressChanged(String deviceAddress, int percent, float speed, float avgSpeed, int currentPart, int partsTotal) {

        }

        @Override
        public void onFirmwareValidating(String deviceAddress) {

        }

        @Override
        public void onDeviceDisconnecting(String deviceAddress) {

        }

        @Override
        public void onDeviceDisconnected(String deviceAddress) {

        }

        @Override
        public void onDfuCompleted(String deviceAddress) {

        }

        @Override
        public void onDfuAborted(String deviceAddress) {

        }

        @Override
        public void onError(String deviceAddress, int error, int errorType, String message) {

        }
    };
    private Context context;
    private BluetoothLeScannerCompat scanner;
    private boolean ble_scanning;
    private BleCallBack myble_callback;
    private BluetoothDevice select_device = null;
    private String select_file = null;
    private int mFileType;

    public BleServices(Context context, BleCallBack BleCallBack) {
        this.context = context;
        myble_callback = BleCallBack;
        ble_scanning = false;
    }

    protected void ble_scan() {
        scanner = BluetoothLeScannerCompat.getScanner();
        if (!ble_scanning) {
            ble_scanning = true;
            Log.d("scanner", "start");
            scanner.startScan(ble_scan_callback);
        } else {
            ble_scanning = false;
            Log.d("scanner", "stop");
            scanner.stopScan(ble_scan_callback);
        }
    }

    protected void setSelect_device(BluetoothDevice device) {
        Toast.makeText(context, String.format("select device:%s", device.getName()), Toast.LENGTH_SHORT).show();
        select_device = device;
    }

    protected void setDFUfile(String Path) {
        select_file = Path;
    }

    protected void connect_device() {
        if (select_device == null) {
            Toast.makeText(context, "not set device", Toast.LENGTH_SHORT).show();
            return;
        }
        select_device.connectGatt(context, false, ble_gatt_callback);
    }

    protected void start_DFU() {
        if (select_device == null) {
            Toast.makeText(context, "not set device", Toast.LENGTH_SHORT).show();
            return;
        }
        if (select_device == null) {
            Toast.makeText(context, "not set device", Toast.LENGTH_SHORT).show();
            return;
        }
        final DfuServiceInitiator starter = new DfuServiceInitiator(select_device.getAddress())
                .setDeviceName(select_device.getName())
                .setKeepBond(true);
// If you want to have experimental buttonless DFU feature supported call additionally:
        starter.setUnsafeExperimentalButtonlessServiceInSecureDfuEnabled(true);
// but be aware of this: https://devzone.nordicsemi.com/question/100609/sdk-12-bootloader-erased-after-programming/
// and other issues related to this experimental service.

// Init packet is required by Bootloader/DFU from SDK 7.0+ if HEX or BIN file is given above.
// In case of a ZIP file, the init packet (a DAT file) must be included inside the ZIP file.
        mFileType = DfuServices.TYPE_AUTO;
        if (mFileType == DfuServices.TYPE_AUTO) {
            starter.setZip(select_file);
        }
        final DfuServiceController controller = starter.start(context, DfuServices.class);
// You may use the controller to pause, resume or abort the DFU process.
    }


}
