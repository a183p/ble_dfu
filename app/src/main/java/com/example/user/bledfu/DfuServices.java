package com.example.user.bledfu;

/**
 * Created by user on 2017/5/5.
 */

import no.nordicsemi.android.dfu.DfuBaseService;

import android.app.Activity;
import android.app.Notification;

public class DfuServices extends DfuBaseService {
    @Override
    protected Class<? extends Activity> getNotificationTarget() {
        return NotificationActivity.class;
    }

    @Override
    protected boolean isDebug() {
        // Here return true if you want the service to print more logs in LogCat.
        // Library's BuildConfig in current version of Android Studio is always set to DEBUG=false, so
        // make sure you return true or your.app.BuildConfig.DEBUG here.
        return BuildConfig.DEBUG;
    }
}
