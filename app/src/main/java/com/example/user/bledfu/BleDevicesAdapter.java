package com.example.user.bledfu;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 2017/5/12.
 */

public class BleDevicesAdapter extends BaseAdapter {

    private Context context;
    private List<BluetoothDevice> devices = new ArrayList<>();

    public BleDevicesAdapter(Context context, List<BluetoothDevice> devices) {
        this.context = context;
        this.devices = devices;
    }

    protected void update_devices(List<BluetoothDevice> devices) {
        this.devices = devices;
    }

    @Override
    public int getCount() {
        return devices.size();
    }

    @Override
    public Object getItem(int i) {
        return devices.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.item_device, viewGroup, false);
        TextView name = (TextView) view.findViewById(R.id.txt_device_name);
        TextView address = (TextView) view.findViewById(R.id.txt_device_address);
        name.setText(devices.get(i).getName());
        address.setText(devices.get(i).getAddress());
        return view;
    }


}
