package com.example.user.bledfu;

import android.Manifest;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import no.nordicsemi.android.support.v18.scanner.ScanResult;

public class MainActivity extends AppCompatActivity implements BleCallBack, View.OnClickListener, AdapterView.OnItemClickListener {
    private BleDevicesAdapter BleDevicesAdapter;
    private ListView lv_devices;
    private Button btn_scan, btn_select_file, btn_connect, btn_dfu;
    private BleServices myble_service;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private static final int PERMISSION_REQUEST_COARSE_STORAGE = 2;
    private static final int FILE_SELECT_CODE = 1234;
    private List<BluetoothDevice> devices = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init_view();
    }

    private void init_view() {
        BleDevicesAdapter = new BleDevicesAdapter(this, devices);//context
        lv_devices = (ListView) findViewById(R.id.lv_devices);
        lv_devices.setAdapter(BleDevicesAdapter);
        lv_devices.setOnItemClickListener(this);

        btn_scan = (Button) findViewById(R.id.btn_scan);
        btn_scan.setOnClickListener(this);

        btn_select_file = (Button) findViewById(R.id.btn_select_file);
        btn_select_file.setOnClickListener(this);

        btn_connect = (Button) findViewById(R.id.btn_connect);
        btn_connect.setOnClickListener(this);

        btn_dfu = (Button) findViewById(R.id.btn_dfu);
        btn_dfu.setOnClickListener(this);
    }

    private void choose_file() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermission(int request_code) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            switch (request_code) {
                case PERMISSION_REQUEST_COARSE_LOCATION:
                    if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                    }
                    break;
                case PERMISSION_REQUEST_COARSE_STORAGE:
                    if (this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_COARSE_STORAGE);
                    }
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "get location permission success", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "get location permission fail", Toast.LENGTH_SHORT).show();
                }
                break;
            case PERMISSION_REQUEST_COARSE_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "get storage permission success", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "get storage permission fail", Toast.LENGTH_SHORT).show();
                }
                break;
        }
        return;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_scan:
                requestPermission(PERMISSION_REQUEST_COARSE_LOCATION);
                myble_service = new BleServices(this, this);//context,callback
                myble_service.ble_scan();
                break;
            case R.id.btn_select_file:
                requestPermission(PERMISSION_REQUEST_COARSE_STORAGE);
                choose_file();
                break;
            case R.id.btn_connect:
                myble_service.connect_device();
                break;
            case R.id.btn_dfu:
                myble_service.start_DFU();
                break;
            default:
                break;
        }
    }

    @Override
    public void scaned_devices(ScanResult devices) {
        //Log.d("callback", "called");
        BluetoothDevice device = devices.getDevice();
        if (!this.devices.contains(device)) {
            this.devices.add(device);
        }
        BleDevicesAdapter.update_devices(this.devices);
        BleDevicesAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d("setSelectDevice", devices.get(i).getName());
        myble_service.setSelect_device(devices.get(i));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Toast.makeText(this, "file select success", Toast.LENGTH_SHORT);
                    Uri uri = data.getData();
                    Log.d("choose file", "File Uri: " + uri.toString());
                    // Get the path
                    String path = uri.getPath();
                    Log.d("choose file", "File Path: " + path);
                    myble_service.setDFUfile(path);
                    // Get the file instance
                    // File file = new File(path);
                    // Initiate the upload
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
